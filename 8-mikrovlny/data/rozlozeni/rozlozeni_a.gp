
set dgrid3d 11,11
set contour
set pm3d


data = 'rozlozeni_xyz.txt'

set xlabel "x [cm]"
set ylabel "y [cm]"
set zlabel "U [V]"

set zrange [0:4.5]
set ztics 0,0.5,4.5

set grid


splot data title "data"