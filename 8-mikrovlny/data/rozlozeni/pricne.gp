set encoding utf8

data = "rozlozeni_pricne.txt"

set key top right
set xrange [-30:+30]

set xlabel "x [cm]"
set ylabel "U [V]"

tit_a = "z = 10 cm"
tit_b = "z = 20 cm"
tit_c = "z = 50 cm"

plot\
 data using 1:2:5:6 with xyerrorbars pt 1 title tit_a,\
 data using 1:3:5:6 with xyerrorbars pt 2 title tit_b,\
 data using 1:4:5:6 with xyerrorbars pt 4 title tit_c