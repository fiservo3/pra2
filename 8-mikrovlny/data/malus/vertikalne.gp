set encoding utf8

set samples 100000
set angles degrees

data = "malus_vertikalne.txt"

k=3
q=1
f(x) = k*(sin(x))**4 + q
fit f(x) data using 1:2:3:4 xyerrors via k, q

set key top center
set xrange [-5:185]

set xlabel "Θ [°]"
set ylabel "U [V]"

plot data  using 1:2:3:4 with xyerrorbars title "změřená data",\
[0:180] f(x) title "fit"

