set encoding utf8

set samples 100000
set angles degrees

data = "malus_horizontalne.txt"

k=3
f(x) = k*(sin(x)*cos(x))**2
fit f(x) data using 1:2:3:4 xyerrors via k 

set key top center
set xrange [-5:185]


set xlabel "Θ [°]"
set ylabel "U [V]"

plot data  using 1:2:3:4 with xyerrorbars title "změřená data",\
[0:180] f(x) title "fit"

