set encoding utf8

set samples 100000
set angles degrees

dataA = "sterbina_60.txt"
dataB = "sterbina_40.txt"

set key top right
set xrange [-5:185]

set xlabel "Θ [°]"
set ylabel "U [V]"

plot\
 dataA  using 1:2:3:4 with xyerrorbars title "šířka 6cm",\
 dataB  using 1:2:3:4 with xyerrorbars title "šířka 4cm"
#[0:180] f(x) title "fit"

