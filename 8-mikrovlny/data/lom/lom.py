import numpy as np
import math

data = np.loadtxt("lom.txt")
n=data.shape[0]

z = np.zeros((n, 1))

j = 0
for i in data:
	print(i)
	alfa = math.radians(i[0])
	beta = math.radians(i[1])

	z[j] = math.sin(beta)/math.sin(alfa)
	print(math.sin(beta)/math.sin(alfa))
	j+=1

print(z.mean())
print(z.std())

