set encoding utf8
set samples 10000

data = "bez.txt"
datas = "s.txt"

set key top right
set xrange [4.5:15.5]

c=0
q=1.3
a = 7.5
k=1.65

f(x) = a*abs(sin ((pi/k)*x + q)) + c

fit f(x) datas using 1:2:3:4 xyerrors via a, q, k
set grid

set xlabel "z [cm]"
set ylabel "U [V]"

plot datas using 1:2:3:4 with linespoints title "data", [-15:15] f(x) title "fit"

