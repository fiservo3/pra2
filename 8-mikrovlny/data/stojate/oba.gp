set encoding utf8
set samples 10000

FIT_LIMIT=1.e-14

datab = "bez.txt"
datas = "s.txt"

set key top left
set xrange [4.5:15.5]

cs = 0
qs = 1
as = 10
ks = 1.65

cb = 0
qb = 1.1
ab = 10
kb = 1.65

fbd(x) = ab*abs(sin ((pi/kb)*(x-qb))) + cb
fsd(x) = as*abs(sin ((pi/ks)*(x-qs))) + cs

fit [7.4:14.5] fbd(x) datab using 1:2:3:4 xyerrors via ab, qb, kb
fit [  0:15  ] fsd(x) datas using 1:2:3:4 xyerrors via as, qs, ks

k = (ks + kb)/2

fbd(x) = ab*abs(sin ((pi/k)*(x-qb))) + cb
fsd(x) = as*abs(sin ((pi/k)*(x-qs))) + cs

fit [7.4:14.5] fbd(x) datab using 1:2:3:4 xyerrors via ab, qb
fit [  0:15  ] fsd(x) datas using 1:2:3:4 xyerrors via as, qs

cara(x) = 0



set grid

set xlabel "z [cm]"
set ylabel "U [V]"

sigma_dz = sqrt(0.03522**2 + 0.02211**2 + 0.04**2)

d=2

print (qb-qs + k)
print sigma_dz
print k


z = 5

plot \
datab using 1:2:3:4 with xyerrorbars title "bez desky", \
datas using 1:2:3:4 with xyerrorbars title "s deskou", \
[7.4:14.5] fbd(x) title "fit bez desky",\
[  5.85:15  ] fsd(x) title "fit s deskou" dt 2,\
[z*k + qs:z*k + qb + k] cara(x) lw 3 lc rgb "red" title "Δz"

