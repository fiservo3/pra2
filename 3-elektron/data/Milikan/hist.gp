set encoding utf8

set samples 10000
A = 1e-19
B = 5e-20

naboj = 1.7e-19

sigmaA = 2e-20
sigmaB = 4e-20

#Gauss(x,mu, amplituda, sigma) = amplituda/(sigma*sqrt(2*pi)) * exp( -(x-mu)**2 / (2*sigma**2))

peakA(x) = Gauss(x, naboj, A, sigmaA);
peakB(x) = Gauss(x, 2.0*naboj, B, sigmaB);

peaky(x) = peakA(x)+peakB(x);

#fit peaky(x) "data_sezvejkana_hist.txt" using 1:2 via naboj, A, B, sigmaA, sigmaB

set grid

set ytics 0,1

set xtics 0, 0.6e-1

set xlabel "Q [10^-^1^8 C ]"
set ylabel "Četnost [1]"


plot "data_sezvejkana_hist.txt" using 1:2 with boxes title "naměřené náboje"