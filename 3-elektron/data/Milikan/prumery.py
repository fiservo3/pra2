import numpy as np;
import os;
import math;

pi = 3.14159265359;

d = 6e-3;
g = 9.81373;


ro_olej = 874;
ro_vzduch = 1.204; #OPRAV TO!!!!!!!!!!!!!!!!!!!!!!!!1
eta = 1.8e-5;

A=6.18e-5/747.1

data = np.loadtxt("data.txt");
n=data.shape[0];

print("nacteno " + str(n) + " hodnot")


z=np.zeros((n, 2))
i = 0
while i<n:
	U = data[i, 0];
	ts = data[i, 1];
	tk = data[i, 2];
	s = 0.5e-3*data[i, 3];
	vs = s/ts;
	vk = s/tk
	r = math.sqrt((9*eta*vk)/(2*g*(ro_olej-ro_vzduch)));
	print (r);
	z[i,0] = r
	fc = 1+(A/r)
	Qc = (6*d/U)*((pi*eta*(vk+vs)*r)/(fc**(3.0/2.0)))
	print(Qc)
	z[i,1] = Qc;
	i+=1;


print(z)

sirkaBinu = (1/5.0)*(0.1);
pocatekBinu = 0.1;
pocetBinu = 20;




biny = np.histogram(z[:,1], np.arange(pocatekBinu, (pocatekBinu+(pocetBinu+1)*sirkaBinu), sirkaBinu));
m=biny[0].shape[0]
hist = np.zeros((m, 2))

hist[:,1] = biny[0]
hist[:,0] = np.arange((pocatekBinu+sirkaBinu*0.5), (pocatekBinu+pocetBinu*sirkaBinu), sirkaBinu)
m=biny[0].shape[0]
print(m);

np.savetxt("data_sezvejkana_hist.txt", hist);




#os.system("gnuplot ./hist.gp --persist")