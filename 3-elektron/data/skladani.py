import math

konverze = 5.6095887e29

me1 = 1.8e11
sigma_me1 = 0.04e11
me2 = 1.019e11
sigma_me2 =0.007e11

p1 =1/(sigma_me1**2)
p2 =1/(sigma_me2**2)

x = (p1*me1+p2*me2)/(p1+p2)
sigma_me = math.sqrt(1/(p1+p2))

print("%.4g"% x)
print("%.4g"% sigma_me)

naboj = 1.7e-19
sigma_e = 0.1e-19

m=(naboj/x)*konverze
sigma_m = m*math.sqrt((sigma_me/x)**2 + (sigma_e/naboj)**2)

print("%.4g"% m)
print("%.4g"% sigma_m)
