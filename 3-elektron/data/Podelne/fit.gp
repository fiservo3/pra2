set encoding utf8

m_e = 1.8e11

N = 174
el = 0.381
pi = 3.14159265359
mu = 1.25663706e-6
z=0.249


data="NabojPodelny.txt"




f(x) = ( m_e * (1.0/8.0) *((z*mu*N)/(pi * el ))**2) * x
fit f(x) data using ($2**2):1:($4*2*$2):3 xyerrors via m_e
#f(x) = x**2



set xlabel "I^2 [A^2]"
set ylabel "U [V]"

set key top left

plot data using ($2**2):1:($4*2*$2):3 with xyerrorbars title "hodnoty", \
f(x) title "fit"