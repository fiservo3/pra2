import numpy as np
import os
from scipy.signal import medfilt


p0 = 15.49e-3
Rchamber = 0.0172934

Tpokoj = 294
Pplynu = 14
KB = 8.6173303e-5
V = 2*(3.14159265**2)*0.4*0.01

x = np.loadtxt("Vyboj.txt")
n = x.shape[0]

print("nacteno radku: "+ str(n))
dt = 2e-5
mk = 1.1e6 *2
y = np.zeros((n,10))

ne  = 2*(p0/(KB*Tpokoj))
print(ne)
y[:,1] = x[:,0]




y[:,0] = np.arange(0, (n)*dt*1e3, dt*1e3)




j=1
while j<n:
	y[j, 2] =  y[j-1, 2] - dt*x[j, 2]*mk

	j += 1

j = 0
for i in x:
	Icelk = y[j, 2]
	Ul = i[0]
	Ik = Ul*Rchamber
	Ip = Icelk -Ik
	y[j, 3] = Ip
	if(Ip > 0 and Ul> 0):
		Te = (0.7*Ip/Ul)**(2/3)
		y[j, 4] = Te
	Te=y[j, 4]
	P = Ul * Ip
	y[j, 5] = P

	Wth = ne*KB*Te/3
	y[j, 6] = Wth

	j+=1
	if j == 900:
		break


#y[:,2] = - x[:,2]
print("Max Elektronova teplota: ")
print(np.amax(medfilt(y[:,4])))

print("Max prikon [W]: ")
print(np.amax(medfilt(y[:,5])))

print("Max pproud [A]: ")
print(np.amax(medfilt(y[:,3])))

print("Max energie [ev]: ")
print(np.amax(medfilt(y[:,6])))




np.savetxt("zprac.txt", y)
os.system("gnuplot pom.gp --persist")
