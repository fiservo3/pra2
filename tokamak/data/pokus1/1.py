import numpy as np
import os

x = np.loadtxt("1.txt")
n = x.shape[0]
dt = 1/(125e6)
y = np.zeros((n,5))

y[:,0] = np.arange(0, n*dt, dt)
y[:,1] = x[:,0]
y[:,4] = x[:,3]

y[0,2] = x[0, 1]*dt
y[0,3] = -x[0, 2]*dt

j=1
while j<n:
	y[j, 2] = y[j-1, 2] + dt*x[j, 1]
	y[j, 3] = y[j-1, 3] - dt*x[j, 2]
	j += 1

np.savetxt("zprac.txt", y)
os.system("gnuplot pom.gp --persist")
