set encoding utf8

data = 'zprac.txt'

set multiplot layout 4, 1 margins 0,0,0,0 spacing 0,0


set ytics


unset title
set key center right
set xtics scale 0
plot data using 1:2 w l title "U _{loop}"
#
unset title
set key center right
plot  data using 1:3 w l title  "B_{tor}"
#
unset title
set key center right
plot data using 1:4 w l title "I_{total}"
#
set xtics
unset title
set key center right
plot  data using 1:5 w l title "Int"

