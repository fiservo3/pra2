import sys
import numpy as np
import matplotlib.pyplot as plt
import scipy.signal as scisig
import urllib2

def downl_save_prak(shot,OSC):
    """
    Checks if file with data from 4 channel rigol osciloscopes (a,b,c,d according to your choice) has been already
    downloaded. If not, it downloads it and saves as txt and returns all channels
    as a numpy array for plotting
    
    Arguments:
    shot -- discharge number
    OSC -- oscilloscope specification letter ('a', 'b', 'c' or 'd')
    """
    
    if OSC=='a':
        oscname='//DAS/0416RigolDS1074{}.ON/data_all'.format(OSC)
    elif OSC=='b':
        oscname='//DAS/0416RigolDS1074{}.ON/data_all'.format(OSC)
    elif OSC=='c':
        oscname='//DAS/0417RigolDS1074{}.ON/data_all'.format(OSC)
    elif OSC=='d':
        oscname='//DAS/0417RigolDS1074{}.ON/data_all'.format(OSC)    
    else:
        print("Unknown oscilloscope or missing data in this discharge")
        sys.exit(1)
        
        
    diag='prak_osc_{}'.format(OSC)
    url='http://golem.fjfi.cvut.cz/shots/'+ str(shot) + oscname
    try:
        X=np.loadtxt('Golem_{}_{}.txt'.format(shot, diag))
    except:
        print("Downloading {}".format(url))
        file=urllib2.urlopen(url)
        X=np.loadtxt(file)
        np.savetxt('Golem_{}_{}.txt'.format(shot, diag),X)
    
    print('Dimensions of the array: {}'.format(np.shape(X)))
    return X

#Call funcion and do some simple plotting
        
X=downl_save_prak(23862,'d')    
fig,ax=plt.subplots(nrows=3)
ax[0].plot(X[:,0])
ax[1].plot(np.cumsum(X[:,1]))
ax[2].plot(np.cumsum(X[:,2]))


plt.show()