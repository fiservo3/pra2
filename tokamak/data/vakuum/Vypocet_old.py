import numpy as np
import os


def offset(data, n):
	i = 0
	suma = 0
	while i<n:
		suma += data[i]
		i+=1
	offset = suma/n
	print ("offset: " + str(offset))
	return offset



x = np.loadtxt("vybojN.txt")
n = x.shape[0]
print("nacteno radku: "+ str(n))
dt = 2e-6
mk = 1.1e7
y = np.zeros((n,4))


y[:,1] = x[:,0]
n_offset = 20

x[:, 2] = -x[:, 2] - offset(x[:, 2], n_offset)
x[:, 1] = x[:, 1] - offset(x[:, 3], n_offset)


y[:,0] = np.arange(0, n*dt*1e4, dt*1e4)




j=1
while j<n:
	y[j, 2] =  y[j-1, 2] + dt*x[j, 2]*mk
	y[j, 3] =  y[j-1, 3] + dt*x[j, 1]
	j += 1


n_offset = 200
y[:, 2] = y[:, 2] + y[:, 3]*10000000


#y[:,2] = - x[:,2]

np.savetxt("zprac.txt", y)
os.system("gnuplot fit.gp --persist")
