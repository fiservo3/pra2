set encoding utf8

data = 'zprac.txt'

set ytics
set y2tics

#set xlabel "U_l [V]"
#set ylabel "I_{ch} [A]"

set xlabel "t [ms]"
set ylabel "U_l [V]"
set y2label "I_{ch} [A]"


R = 1

f(x) = x/R

fit f(x)  data using 2:3 via R

#set xrange[0:5]
#plot\
# data using 1:2 axes x1y1 with line title "B",\
# data using 1:3 axes x1y2 with line title "I_k",\

#plot\
 data using 2:3 axes x1y1 title "data",\
 f(x) title "fit"
