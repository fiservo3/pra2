set encoding utf8

data = 'sinusx.page'
x=0
dx=.1
plot \
data using 1:2 w l title "sin(x)",\
data using 1:(x=x+$2*dx) w l title "int sin(x) dx"
