import numpy as np;
import os;


Ikal = 62
Rchamber = 0.0075
Tpokoj = 21
Pplynu = 14
KB = 8.6173303e-5
V = 2*(3.14159265**2)*0.4*0.01

data = np.loadtxt("Vyboj.txt");

m=data.shape[0];

print("nacteno dat:")
print (m)

zA=np.zeros((m, 11))

i = 0;
while i<m:
	zA[i,0] = 20*i;
	i += 1;


zA[0,1] = data[0, 2]*Ikal;

i = 1;
while i<m:
	zA[i,1] = zA[i-1,1] + data[i, 2]*Ikal;
	i += 1;


i = 0;
while i<m:
        zA[i,2] = zA[i,1] - data[i, 1]/Rchamber;
        i += 1;

i = 0;
while i<m:
        zA[i,3] = data[i,0]/zA[i,1];
        i += 1;

i = 0;
while i<m:
        if data[i,0] == 0:
                zA[i,4] = 0;
        else:
                zA[i,4] = (0.7*zA[i,2]/data[i,0])**(2/3.0);

        if zA[i,4] > 100:
                zA[i,4] = 0;
        zA[i,8] = zA[i,4];
        i += 1;

i = 0;
while i<m:
        zA[i, 5] = data[i, 0]*zA[i,2];
        zA[i, 9] = zA[i,5];
        i += 1;

n = 2*Pplynu/Tpokoj/KB

i = 0;
while i<m:
        zA[i,6] = n/3*KB*zA[i,4]*V;
        zA[i,10] = zA[i,6];
        i += 1;


zA[0,7] = data[0,0];
i = 1;
while i<m:
        zA[i,7] = data[i,0];
        if zA[i,7]-zA[i-1,7] > 8:
                zA[i,7] = 0;
        i += 1;

PlazmaStart = 0
PlazmaEnd = 0

i = 0;
while i<m:
        if zA[i,7] > 8:
                PlazmaStart = i;
                break;
        i += 1;


while i<m:
        if zA[i,7] > 20:
                PlazmaEnd = i;
                break;
        i += 1;

MaxIP = 0
Temp = 0

i = PlazmaStart;
while i<PlazmaEnd:
        if zA[i,2] > Temp:
                Temp = zA[i,2];
        i += 1;

MaxIP = Temp;
Temp = 0;


i = PlazmaStart;
while i<PlazmaEnd:
        if abs(zA[i,8]-zA[i-1,8]) > 8:
                zA[i,8] = zA[i-1,8];
        i += 1;

i = PlazmaStart;
while i<PlazmaEnd:
        if zA[i,8] > Temp:
                Temp = zA[i,8];
        i += 1;

MaxTe = Temp
Temp = 0;

i = PlazmaStart;
while i<PlazmaEnd:
        if abs(zA[i,9]-zA[i-1,9]) > 15000:
                zA[i,9] = zA[i-1,9];
        i += 1;

i = PlazmaStart;
while i<PlazmaEnd:
        if zA[i,9] > Temp:
                Temp = zA[i,9];
        i += 1;

MaxPoh = Temp
Temp = 0;

i = PlazmaStart;
while i<PlazmaEnd:
        if abs(zA[i,10]-zA[i-1,10]) > 0.4:
                zA[i,10] = zA[i-1,10];
        i += 1;

bodMax = 0
i = PlazmaStart;
while i<PlazmaEnd:
        if zA[i,10] > Temp:
                Temp = zA[i,10];
                bodMax = i;
        i += 1;

bodMax *= 20;
maxWth = Temp
Temp = 0;


np.savetxt("Data.txt", zA);
os.system("gnuplot ./pomocny_plot.gp --persist")

print("Doba trvani plazmatu: Od:")
print(PlazmaStart*20)
print("Do: ")
print(PlazmaEnd*20)
print("Max proud plazmatem: ")
print(MaxIP)
print("Max Elektronova teplota: ")
print(MaxTe)
print("Max Ohmicky prikon: ")
print(MaxPoh)
print("Max Energie Plazmatu: ")
print(maxWth)
print("Cas maxima: ")
print(bodMax)
