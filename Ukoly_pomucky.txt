
2-Hystereze

1
 Pracovní úkoly
1. DÚ: Zjistěte, jak určíte koercitivní sílu,  remanenci a magnetizační ztráty. (např. [3] str. 53 - 57)
2. Změřte hysterezní smyčku toroidu z dané feromagnetické látky a graficky ji znázorněte.
3. Určete koercitivní sílu HK, remanenci BR a magnetizační ztráty.
4. Diskutujte, jak magnetické pole země ovlivňuje měření a zda-li je možné jej s danou apa-
raturou měřit.
2
 Pomůcky
Pomůcky: Balistický galvanometr, 2 odporové dekády, odporový normál, toroidální cívka, vypínač,
rezistor, 2 přepínače, 1 komutátor, digitální multimeter, stabilizovaný zdroj, normál vzájemné
indukčnosti.


3a-Měrný náboj

1
 Pracovní úkoly
1. DÚ: Odvoďte vztah (15), spočtěte B pro U = 100 V (dosazujte energii v jednot-
kách keV!) a diskutujte, zda je korektní považovat elektrony v této úloze za
nerelativistické.
2. DÚ: Odvoďte vztahy (17) a (19) (stačí ponechat v domácí přípravě).
3. Změřte měrný náboj elektronu působením podélného magnetického pole. Měření proveďte
pro různé hodnoty urychlovacího napětí U v rozmezí 750 až 1250 V. Pomocné napětí na
A1 (Obr. 7) volte 140 V. Hodnotu e/me určete fitováním závislosti (17) s errorbary.
4. Změřte měrný náboj elektronu působením kolmého magnetického pole. Naměřte několik
dvojic urychlovacích napětí U (v rozsahu do 300 V) a magnetizačního proudu I (v rozsahu
do 4 A). Hodnotu e/me určete fitováním závislosti (19) s errorbary.
2
 Pomůcky
Pomůcky: Regulovatelné zdroje napětí: 300 V a 2 kV, regulovatelný zdroj proudu 10 A, zdroj
střídavého napětí 6,3 V, ampérmetr, voltmetr, obrazovka se solenoidem, katodová trubice, Hel-
mholtzovy cívky, aparatura na měření průměru elektronového svazku s dřevěnými posuvnými
měřidly a zástěnou, tyčový a podkovovitý permanentní magnet.


3b-Milikanův experiment

1
 Pracovní úkoly
1. DÚ: Odvoďte vztah (10) pro výpočet náboje kapky.
2. Proveďte Millikanův experiment pro alespoň deset kapiček oleje. Výsledky zpracujte formou
grafu Q na r a určete elementární náboj.
3. Z výsledků úlohy 3a (Měrný náboj elektronu) a této stanovte hmotnost elektronu, vyjádřete
v jednotkách keV/c2 .
2
 Pomůcky
Pomůcky: Millikanův přístroj HELAGO 559 412, napájecí jednotka HELAGO 559 421, 2x
elektronické stopky HELAGO 313 033, vodiče, olej.


4-Balmerova serie

1
 Pracovní úkoly
1. DÚ: V přípravě odvoďte vzorec (11) pro případ, kdy je splněna podmínka úhlu
nejmenší deviace a1 = a2 .
2. Metodou dělených svazků změřte lámavý úhel hranolu. Měření opakujte 5×.
3. Změřte index lomu hranolu v závislosti na vlnové délce pro čáry rtut’ového spektra, vyneste
do grafu a fitováním nelineární funkcí (13) určete disperzní vztah n = n(l).
4. Změřte vlnové délky spektrálních čar zinkové výbojky a porovnejte je s tabulkovými hodno-
tami.
5. Změřte spektrum vodíkové výbojky, porovnejte s tabulkovými hodnotami, ověřte platnost
vztahu (5) a určete hodnotu Rydbergovy konstanty.
dn
6. Určete charakteristickou disperzi v okolí vlnové délky 589 nm (žlutá čára v sodíkovém
dl spektru). Poté spočíťejte minimální velikost základny hranolu, vyrobeného ze stejného materiálu
jako hranol, se kterým měříťe, který je ještě schopný sodíkový dublet rozlišit.
2
 Pomůcky
Pomůcky: Goniometr, hranol (délka hrany 3 cm), stolní lampa, rtut’ová, zinková, vodíková a
sodíková výbojka.


5-Teplota wolframového vlánka

1
 Pracovní úkoly
1. DÚ: Z Planckova vyzařovacího zákona odvoďte Stefan-Boltzmannův zákon a určete
tvar konstanty s pomocí c , k a h.
2. Ocejchujte referenční žárovku pomocí měření odporu. Diskutujte, zda a v rovnici (9) je kon-
stanta. Výsledky zpracujte graficky. Ověřte správnost výsledků pomocí závislosti výkonu na čtvrté
mocnině teploty. Pomocí fitu určete konstantu B.
3. Ověřte Stefan-Boltzmanův zákon (7), výsledky vyneste do grafu a určete konstatu .
4. Zjistěte teplotu referenční žárovky (alespoň 6 měření) pomocí závislosti transmise na vlnové
délce. Graficky zpracujte a teplotu získejte pomocí aritmetického průměru z fitů závislosti inten-
zity na vlnové délce I = I(l).
2
 Pomůcky
Pomůcky: Pulfrichův fotometr, zdroj napětí 0 - 30 V, wolframová vlákna (dvě světelné žárovky),
multimetr, ohmmetr, zdroj referenčního napětí.


6-Geometrická optika

1
 Pracovní úkoly
1. DÚ: V přípravě odvoďte rovnici 5, načrtněte chod paprsků a zdůvodněte nutnost
podmínky e > 4f . Vysvětlete rozdíl mezi Galileovým a Keplerovým dalekohledem.
Zjistěte, co je konvenční zraková vzdálenost.
2. Určete ohniskovou vzdálenost spojné čočky +200 ze znalosti polohy předmětu a jeho obrazu
(pro minimálně pět konfigurací, proveďte též graficky) a Besselovou metodou.
3. Změřte ohniskovou vzdálenost mikroskopického objektivu a Ramsdenova okuláru Besselovou
metodou. V přípravě vysvětlete rozdíl mezi Ramsdenovým a Huygensovým okulárem.
4. Změřte zvětšení lupy při akomodaci oka na konvenční zrakovou vzdálenost. Stanovte z ohnis-
kové vzdálenosti lupy zvětšení při oku akomodovaném na nekonečno.
5. Určete polohy ohniskových rovin tlustých čoček (mikroskopický objektiv a Ramsdenův okulár)
nutných pro výpočet zvětšení mikroskopu.
6. Z mikroskopického objektivu a Ramsdenova okuláru sestavte na optické lavici mikroskop a
změřte jeho zvětšení.
7. Ze spojky +200 a Ramsdenova okuláru sestavte na optické lavici dalekohled. Změřte jeho
zvětšení přímou metodou.
8. Výsledky měření zvětšení mikroskopu a dalekohledu porovnejte s hodnotami vypočíťanými
z ohniskových vzdáleností.


7-Gama spektrometrie

1
 Pracovní úkoly
1. DÚ: Pomocí rovnice (1) sestavte diferenciální rovnici a jejím řešením odvoďte
zákon radioaktivního rozpadu (2). S jeho pomocí dále podle definice odvoďte vztah
(3) pro poločas rozpadu.
2. Osciloskopem pozorujte spektrum 137 Cs na výstupu z jednokanálového analyzátoru. Načrtněte
tvar spektra (závislost intenzity na energii záření) a přiložte k protokolu. (Osciloskop ukazuje
tvary a amplitudy jednotlivých pulzů. Počet pulzů je dán intenzitou čáry a energie výškou im-
pulzu.)
3. Naměřte spektrum impulzů 137Cs jednokanálovým analyzátorem pomocí manuálního měření.
Okno volte o šířce 100 mV (10 malých dílků). Spektrum graficky zpracujte.
4. Mnohokanálovým analyzátorem naměřte jednotlivá spektra přiložených zářičů (137Cs, 60 Co,
241
Am a 133Ba). Určete výrazné píky a porovnejte je s tabulkovými hodnotami. (Každé spektrum
nabírejte 10 minut. Před zpracováním odečtěte pozadí - viz úkol 9.)
5. Pomocí zářičů 137 Cs a 60 Co určete kalibrační křivku spektrometru a použijte ji při zpracování
všech spekter naměřených mnohokanálovým analyzátorem. (Spektrum nemusíťe nabírat znovu,
použijte data z předchozího měření.)
6. S využitím všech naměřených spekter určete závislost rozlišení spektrometru na energii gama
záření. (Je definováno jako poměr šířky fotopíku v polovině jeho výšky k jeho energii - viz
poznámka.)
7. Z naměřeného spektra 137 Cs určete hodnotu píku zpětného rozptylu, Comptonovy hrany,
energii rentgenového píku a energii součtového píku.
8. Mnohokanálovým analyzátorem naměřte spektrum neznámého zářiče. Určete tento zářič, po-
zorujte a zaznamenejte další jevy v jeho spektru. (Spektrum nabírejte 10 minut.)
9. Mnohokanálovým analyzátorem naměřte spektrum pozadí v místnosti (zářiče uschovejte do
trezoru). Najděte v pozadí přirozené zářiče a toto pozadí odečtěte od všech zaznamenaných
spekter ještě před jejich vyhodnocením. (Pozadí nabírejte 10 minut.)
10. Graficky určete závislost koeficientu uťlumu olova na energii gama záření. (Použijte zářiče
137
Cs, 60 Co a 133 Ba současně, jednotlivá spektra nabírejte 10 minut.)


8-Mikrovlny

1
 Pracovní úkoly
1. DÚ: Odvoďte vztah 1 pro výpočet polohy interferenčních maxim při difrakci na
mřížce.
l
(sin T max = m , m = 0, ±1, . . .
 (1)
d
2. Oveřte, že pole před zářičem je lineárně polarizované a určete směr polarizace. Ověřte Malusův
zákon pro danou polarizační mřížku. Sestrojte dva grafy závislosti přijímaného napětí U na úhlu
pootočení polarizační mřížky T nejprve pro sondu vertikálně a potom horizontálně.
3. Proměřte rozložení elektromagnetického pole v rovině před zářičem a zobrazte jeho prostorový
graf ve vhodném programu (GNUplot, Mathematica, Matlab). Do protokolu zpracujte podélné
a příčné rozložení pole (nezávislou veličinou budou souřadnice a závislou velikost napětí).
4. Demonstrujte a proměřte stojaté vlnění. Z rozložení pole určete vlnovou délku l. V druhé
části pokusu vložte dielektrickou desku do pole stojaté vlny a pomocí vztahů odvozených v
postupu stanovte index lomu dielektrické desky n.
5. Ověřte kvazioptické chování mikrovln - difrakce na hraně, štěrbině a překážce, zákon lomu
a fokusace čočkou. Spočíťejte vlnovou délku l z grafu vlnění na štěrbině a index lomu cukru n
pomocí ohniskové vzdálenosti čočky. Sestrojte příslušné grafy.
6. Ověřte šíření mikrovln pomocí Lecherova vedení a vlnovodu. Ověřte, že podél Lecherova
vedení se šíří stojatá vlna a určete z ní vlnovou délku l.
2
 Pomůcky
Pomůcky: Gunnův oscilátor 737 01, sonda elektrického pole 737 35, zdroj napětí se zesilovačem
737 020, trychtýřovitý nástavec 737 21, tyč 240 mm 737 15, transformátor 220V/12V 562 791, 2
BNC kabely 737 01, USB link PASCO 2100, osobní počíťač, program pro datový sběr Data Stu-
dio, kartonová souřadnicová síť’, polarizační mřížka, 2 držáky na desky, 2 kovové desky 230 × 230
mm, dielektrická deska PVC 20 mm, kovová deska 230×60 mm, pravíťko, dutý půlválec, dřevěný
držák půlválce, ”A” podstava, konvexní čočka, Lecherovo vedení + kovová spojka, kovový vl-
novod, technický cukr, stojan s držáky.


9-Polarizace

1
 Pracovní úkoly
1. DÚ: Odvoďte vzorec (1) pro Brewsterův úhel úplné polarizace. Vycházejte z
Obr. 1 a ze zákona lomu světla na rozhraní dvou optických prostředí. Spočtěte
Brewsterův úhel pro rozhraní vzduch - skleněné zrcadlo. Při měření Brewsterova
úhlu se doporučuje míť připravenou tabulku v Excelu pro výpočet stupně polarizace.
2. Při polarizaci bílého světla odrazem na černé skleněné desce proměřte závislost stupně polar-
izace na sklonu desky a určete optimální hodnotu Brewsterova úhlu. Výsledky zaneste do grafu
a porovnejte s vypočtenou hodnotou z domácího úkolu.
3. černou otočnou desku nahraďte polarizačním filtrem a proměřte závislost intenzity polarizo-
vaného světla na úhlu otočení analyzátoru (Malusův zákon). Výsledek srovnejte s teoretickou
předpovědí, znázorněte graficky a výsledek diskutujte.
4. Na optické lavici prozkoumejte vliv čtyř celofánových dvojlomných filtrů, způsobujících inter-
ferenci. Vyzkoušejte vliv otáčení analyzátoru vůči polarizátoru a vliv otáčení dvojlomného filtru
mezi zkříženými i rovnoběžnými polarizátory v bílém světle. Pozorováním zjistěte, které vlnové
délky (barvy) se interferencí zvýrazní. Výsledky pozorování popište.
5. Pomocí dvou polarizačních filtrů, fotočlánku a barevných filtrů změřte měrnou otáčivost
křemene s tloušťkou 1 mm pro 4 vlnové délky světla. Jakou závislost pozorujete mezi vlnovou
délkou světla a měrnou otáčivostí? Naměřené hodnoty porovnejte s tabulkovými. Jak se změní
výsledek když použijete křemenný vzorek s větší tloušťkou? Diskutujte naměřené výsledky.
2
 Pomůcky
Pomůcky: Optická lavice, otočné černé zrcadlo, 2x polarizační filtr, multimetr, otočný držák
pro dvojlomný vzorek, čtvrtvlnná destička, světelný zdroj s matnicí, fotočlánek, 4 celofánové
dvojlomné filtry, 4 barevné filtry, křemenný klín.


10-Interference

1
 Pracovní úkoly
1. DÚ: Ve vztazích (9), (13) a (18) vyjádřete sin T pomocí polohy maxima/minima
od středu a uražené dráhy laserového paprsku.
2. Změřte průměr tří nejmenších kruhových otvorů užitím Fraunhoferovy difrakce světla s
pomocí měřicího mikroskopu a výsledky srovnejte. Odhadněte chybu měření šířky štěrbiny
mikroskopem. Pro který průměr kruhového otvoru je přesnější měření interferencí a pro
který mikroskopem?
3. Změřte 10 různých šířek štěrbiny užitím Fraunhoferovy difrakce světla a srovnejte s hod-
notou na mikrometrickém šroubu. Pro jaké šířky štěrbiny je výhodnější měření interferencí
a pro jaké mikrometrickým šroubem?
4. Změřte mřížkovou konstantu optické mřížky a srovnejte s hodnotou uvedenou na mřížce.
5. Sestavte Michelsonův interferometr a změřte vlnovou délku laserového svazku.
2
 Pomůcky
Pomůcky: železná deska s magnetickými stojánky, He-Ne laser (633 nm, 5 mW), 2 zrcadla na
stojánku, optická lavice s jezdci, 2 spojky (+50, +200), rozptylka (-100), sada kruhových otvorů,
nastavitelná štěrbina s mikrometrickým šroubem, stojan na mřížku, optická mřížka, stínítko na
zdi, stínítko, držák na stínítko a kruhové otvory, pásmové měřidlo (5 m), pravítko 20 cm a 30 cm,
měřicí mikroskop, Abbeho kostka, rovinné zrcadlo s mikrometrickým šroubem, rovinné zrcadlo,
provázek.
