set encoding utf8
set samples 1000

data = 'cs_prepocteno.txt'

energieA = 700
sigmaA = 100
velikostA = 4500
konstA = 100

energieB = 450
sigmaB = 100
velikostB = 1200
konstB = 150

energieC = 450
sigmaC = 100
velikostC = 1200
konstC = 900


#
Gauss(x,mu, amplituda, sigma, konst) = amplituda * exp( -(x-mu)**2 / (2*sigma**2)) + konst;
#
peakA(x) = Gauss(x, energieA, velikostA, sigmaA, konstA);
fit [600:800] peakA(x) data via energieA, sigmaA, velikostA


peakB(x) = Gauss(x, energieB, velikostB, sigmaB, konstB);
fit [430:520] peakB(x) data via energieB, sigmaB, velikostB

peakC(x) = Gauss(x, energieC, velikostC, sigmaC, konstC);
fit [190:270] peakC(x) data via energieC, sigmaC, velikostC


set xlabel "e [keV]"
set ylabel "pulzy [(10 min)^-^1]"

set xrange [0:1000]

plot data with boxes title "naměřené spektrum" lw 0.5,\
peakA(x) lw 0.8 lc rgb "dark-green" dt 4 notitle, [600:800] peakA(x) lw 2  lc rgb "dark-green" dt 4 title "fit 1",\
peakB(x) lw 0.8 lc rgb "red" dt 5 notitle, [430:520] peakB(x) lw 2  lc rgb "red" dt 5 title "fit 2",\
peakC(x) lw 0.8 lc rgb "blue" dt 7 notitle, [190:270] peakC(x) lw 2  lc rgb "blue" dt 7 title "fit 3",\
