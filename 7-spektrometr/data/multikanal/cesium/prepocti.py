import numpy as np

def energie(kanal):
	#return(4.35719 * kanal -60.8113)
	return(4.14492 * kanal)



cesium = np.loadtxt("cesium")
pozadi = np.loadtxt("pozadi")

n = cesium.shape[0]
prepocteny = np.zeros((n, 2))

for i in np.arange(0, n):
	prepocteny[i, 0] = energie(cesium[i, 0])
	pulzy = cesium[i, 1] - pozadi[i, 1] 
	if pulzy < 0:
		pulzy = 0
	prepocteny[i, 1] = pulzy


np.savetxt("cs_prepocteno.txt", prepocteny)