set encoding utf8
set samples 1000

data = 'am_prepocteno.txt'

energieA = 120
sigmaA = 30
velikostA = 50
konstA = 0

#
Gauss(x,mu, amplituda, sigma, konst) = amplituda * exp( -(x-mu)**2 / (2*sigma**2)) + konst;
#
peakA(x) = Gauss(x, energieA, velikostA, sigmaA, konstA);
fit [40:180] peakA(x) data via energieA, sigmaA, velikostA

set xlabel "e [keV]"
set ylabel "pulzy [(10 min)^-^1]"
#set logscale y

set xrange [0:2000]

plot data with boxes title "naměřené spektrum" lw 0.5,\
peakA(x) lw 0.8 lc rgb "dark-green" dt 4 notitle, [40:180] peakA(x) lw 2  lc rgb "dark-green"  title "fit",\