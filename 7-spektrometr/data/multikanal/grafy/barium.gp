set encoding utf8
set samples 1000

data = 'ba_prepocteno.txt'


energieA = 100
sigmaA = 100
velikostA = 4500
konstA = 60

energieB = 450
sigmaB = 100
velikostB = 1200
konstB = 120

Gauss(x,mu, amplituda, sigma, konst) = amplituda * exp( -(x-mu)**2 / (2*sigma**2)) + konst;
#
peakA(x) = Gauss(x, energieA, velikostA, sigmaA, konstA);
fit [80:110] peakA(x) data via energieA, sigmaA, velikostA


peakB(x) = Gauss(x, energieB, velikostB, sigmaB, konstB);
fit [330:400] peakB(x) data via energieB, sigmaB, velikostB

set xlabel "e [keV]"
set ylabel "pulzy [(10 min)^-^1]"
#set logscale y

set xrange [0:600]

plot data with boxes title "naměřené spektrum" lw 0.5,\
peakA(x) lw 0.8 lc rgb "dark-green" dt 4 notitle, [80:110]  peakA(x) lw 2  lc rgb "dark-green" dt 4  title "fit",\
peakB(x) lw 0.8 lc rgb "red" dt 5 notitle, [330:400] peakB(x) lw 2  lc rgb "red" dt 5 title "fit 2"
