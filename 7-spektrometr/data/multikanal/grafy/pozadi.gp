set encoding utf8
set samples 1000000

data = 'pozadi'


set xlabel "kanál [-]"
set x2label "e [keV]"
set ylabel "pulzy [(10 min)^-^1]"

set xrange [0:4000]
set x2range [0:16579.68]
set x2tics

set logscale y

plot data with boxes title "pozadi" lw 0.5