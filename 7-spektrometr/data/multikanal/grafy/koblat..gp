set encoding utf8
set samples 1000

data = 'co_prepocteno.txt'


energieA = 1350
sigmaA = 100
velikostA = 4500
konstA = 500

energieB = 1200
sigmaB = 100
velikostB = 1200
konstB = 2000

energieC = 900
sigmaC = 100
velikostC = 1200
konstC = 2300


Gauss(x,mu, amplituda, sigma, konst) = amplituda * exp( -(x-mu)**2 / (2*sigma**2)) + konst;
#
peakA(x) = Gauss(x, energieA, velikostA, sigmaA, konstA);
fit [1280:1370] peakA(x) data via energieA, sigmaA, velikostA


peakB(x) = Gauss(x, energieB, velikostB, sigmaB, konstB);
fit [1130:1220] peakB(x) data via energieB, sigmaB, velikostB

peakC(x) = Gauss(x, energieC, velikostC, sigmaC, konstC);
fit [820:960] peakC(x) data via energieC, sigmaC, velikostC
7
set xlabel "e [keV]"
set ylabel "pulzy [(10 min)^-^1]"
#set logscale y

set xrange [0:1500]

plot data with boxes title "naměřené spektrum" lw 0.5,\
peakA(x) lw 0.8 lc rgb "dark-green" dt 4 notitle, [1280:1370]  peakA(x) lw 2  lc rgb "dark-green" dt 4  title "fit",\
peakB(x) lw 0.8 lc rgb "red" dt 5 notitle, [1130:1220] peakB(x) lw 2  lc rgb "red" dt 5 title "fit 2",\
peakC(x) lw 0.8 lc rgb "blue" dt 7 notitle, [820:960] peakC(x) lw 2  lc rgb "blue" dt 7 title "fit 3"

