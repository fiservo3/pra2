set encoding utf8


kanalA = 280
kanalB = 315
sigmaA = sigmaB = 10
velikostA = 3000
velikostB = 3000
konstA = konstB = 2000

#Gauss(x) = amplituda/(sigma*sqrt(2*pi)) * exp( -(x-mu)**2 / (2*sigma**2)) + konst

Gauss(x,mu, amplituda, sigma, konst) = amplituda * exp( -(x-mu)**2 / (2*sigma**2)) + konst;

peakA(x) = Gauss(x, kanalA, velikostA, sigmaA, konstA);
peakB(x) = Gauss(x, kanalB, velikostB, sigmaB, konstB);


fit[270:295] peakA(x) 'kobalt_odecteny.txt' via kanalA, sigmaA, velikostA
fit[310:330] peakB(x) 'kobalt_odecteny.txt' via kanalB, sigmaB, velikostB

set xlabel "kanal [-]"
set ylabel "pulzy [1/(10 min)]"

set xrange [240:350]
set yrange [1500:4000]

plot\
'kobalt_odecteny.txt' with boxes title "naměřené spektrum",\
peakA(x) lw 0.3 lc rgb "red" dt 3 notitle, [270:295] peakA(x) lw 2  lc rgb "red" dt 3 title "fit 1" ,\
peakB(x) lw 0.3 lc rgb "dark-green" dt 4 notitle, [310:330] peakB(x) lw 2  lc rgb "dark-green" dt 4 title "fit 2"
