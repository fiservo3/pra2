#tento skript poustet rucne pouze pred kalibraci, pro dalsi zpracovani jiz ne

import numpy as np
pozadi = np.loadtxt("pozadi")
kobalt = np.loadtxt("kobalt")
cesium = np.loadtxt("cesium")

cesium = cesium[:, 1] - pozadi[:, 1]
kobalt = kobalt[:, 1] - pozadi[:, 1]

np.savetxt("kobalt_odecteny.txt", kobalt)
np.savetxt("cesium_odecteny.txt", cesium)