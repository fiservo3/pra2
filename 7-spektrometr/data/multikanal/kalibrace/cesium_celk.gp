set encoding utf8
set samples 1000

kanalA = 165
sigmaA = 5
velikostA = 4500
konstA = 100

Gauss(x,mu, amplituda, sigma, konst) = amplituda * exp( -(x-mu)**2 / (2*sigma**2)) + konst;

peakA(x) = Gauss(x, kanalA, velikostA, sigmaA, konstA);
fit[150:182] peakA(x) 'cesium_odecteny.txt' via kanalA, sigmaA, velikostA

set xlabel "kanal [-]"
set ylabel "pulzy [1/(10 min)]"

set xrange [80:250]

plot 'cesium_odecteny.txt' with boxes title "naměřené spektrum" lw 0.5,\
peakA(x) lw 1 lc rgb "dark-green" dt 4 notitle, [150:182] peakA(x) lw 2  lc rgb "dark-green" dt 4 title "fit gaussovy funkce"
