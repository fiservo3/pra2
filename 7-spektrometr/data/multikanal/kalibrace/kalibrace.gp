set encoding utf8
set xlabel "kanál [-]"
set ylabel "energie [keV]"

k = 10
k2 = 10
q = -100

f(x) = k*x# + q
g(x) = k2*x + q
fit f(x) 'tabulka.txt' using 1:3:2:4 xyerror via k#, q 
fit g(x) 'tabulka.txt' using 1:3:2:4 xyerror via k2, q 


set key top left
set grid
plot\
'tabulka_co.txt' using 1:3:2:4 with xyerrorbars title "60 Co ",\
'tabulka_cs.txt' using 1:3:2:4 with xyerrorbars title "137 Cs" pt 4,\
f(x) title "fit y = kx",\
g(x) title "fit y = mx + q" dt 2
