set encoding utf8
set samples 1000

data = 'prepocteno.txt'
d=0.00311


k = q = 42

f(x) = k*x + q

fit [500:3000] f(x) data using 1:(-log($2/$3)/d) via k, q

set xlabel "e [keV]"
set ylabel "μ"
#set logscale y

set key top left

set xrange [0:3500]

plot data using 1:(-log($2/$3)/d) with points title "naměřené μ" lw 0.4,\
f(x) lc rgb "blue" lw 0.5 notitle,[500:3000] f(x) lc rgb "blue" lw 1.5 title "fit y = kx + q"