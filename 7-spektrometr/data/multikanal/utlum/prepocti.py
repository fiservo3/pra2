import numpy as np

def energie(kanal):
	#return(4.35719 * kanal -60.8113)
	return(4.14492 * kanal)



bez = np.loadtxt("bezolova")
pozadi = np.loadtxt("pozadi")
s = np.loadtxt("solovem")

n = pozadi.shape[0]
prepocteny = np.zeros((n, 3))

for i in np.arange(0, n):
	prepocteny[i, 0] = energie(bez[i, 0])

	pulzy = s[i, 1] - pozadi[i, 1] 
	if pulzy < 0:
		pulzy = 0
	prepocteny[i, 1] = pulzy

	pulzy = bez[i, 1] - pozadi[i, 1] 
	if pulzy < 0:
		pulzy = 0
	prepocteny[i, 2] = pulzy


np.savetxt("prepocteno.txt", prepocteny)