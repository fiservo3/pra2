set encoding utf8
set ylabel "četnost [s^-^1]"
set xlabel "okno analyzátoru [mV]"

#nevim, jestli to nenechat linearni, vypada to pak taky docela dobre
#set logscale y

plot \
'prepocitano.txt' using 1:2 with boxes lc rgb "blue" title "změřené hodnoty",\
'prepocitano.txt' using 1:2:3 with yerrorbars lc rgb "blue" pt 1 notitle