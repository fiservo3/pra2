set encoding utf8

data = 'sterb_graf.txt'

f(x) = x


set xlabel "D_n [mm]"
set ylabel "D_i [mm]"

set xrange [0.3:1.4]
set grid
set key top left

plot \
data using 1:($2*100000):($3*100000) with yerrorbars title "m=1", \
data using 1:($5*100000):($6*100000) with yerrorbars title "m=2", \
f(x)  title "teoretická hodnota"
