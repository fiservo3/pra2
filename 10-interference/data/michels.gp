set encoding utf8

data = 'michels_int.txt'
 
lambda = 1
f(x) = (2*x)/lambda

fit f(x) data via lambda

set xlabel "ΔX [μm]"
set ylabel "n [-]"

xr = 41*5
set xrange [0:xr]
set x2range [0:100*xr]


set grid
set key top left

plot \
data axes x2y1,\
f(x) axes x2y1 title "fit"

print(2/k)