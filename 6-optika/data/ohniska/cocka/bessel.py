import math
import numpy as np

data =np.loadtxt("bessel.txt")
n=data.shape[0]

hodnoty = np.zeros(n)

for i in np.arange(0,n):
	e = data[i, 0]
	d = data[i, 1]

	f = (e**2-d**2)/(4*e)
	#print (f)
	hodnoty[i] = f

print("f = " + str(hodnoty.mean()) + " +- " + str(hodnoty.std()))
#print(data)