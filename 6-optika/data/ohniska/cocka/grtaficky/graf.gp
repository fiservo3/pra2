set encoding utf8

set samples 500

k1 = k2 = k3 = k4 = k5 = -1
q1 = q2 = q3 = q4 = q5 = 45
	
#a proc bych to pocital jako slusny clovek kdyz mohu jako prase fitnout primku
	g1(x) = k1*x + q1
	fit g1(x) './1.txt' via k1, q1

	g2(x) = k2*x + q2
	fit g2(x) './2.txt' via k2, q2

	g3(x) = k3*x + q3
	fit g3(x) './3.txt' via k3, q3

	g4(x) = k4*x + q4
	fit g4(x) './4.txt' via k4, q4

	g5(x) = k5*x + q5
	fit g5(x) './5.txt' via k5, q5

h(x) = (g1(x)-g2(x))**2 + (g1(x)-g3(x))**2 + (g1(x)-g4(x))**2 + (g1(x)-g5(x))**2 + (g2(x)-g3(x))**2 + (g2(x)-g4(x))**2 + (g2(x)-g5(x))**2 + (g3(x)-g4(x))**2 + (g3(x)-g5(x))**2 + (g4(x)-g5(x))**2
q(x) = x

set xlabel "a [cm]"
set ylabel "a' [cm]"

print ("primky [k, q]")
print k1,", ",q1
print k2,", ",q2
print k3,", ",q3
print k4,", ",q4
print k5,", ",q5



set yrange[0:60]

plot\
 './1.txt' pt 1 lc rgb "black" notitle, g1(x) title "měření 1", \
 './2.txt' pt 1 lc rgb "black" notitle, g2(x) title "měření 2", \
 './3.txt' pt 1 lc rgb "black" notitle, g3(x) title "měření 3", \
 './4.txt' pt 1 lc rgb "black" notitle, g4(x) title "měření 4", \
 './5.txt' pt 1 lc rgb "black" notitle, g5(x) title "měření 5", \
  q(x) lc rgb "black" dt 2  title "osa kvadrantu"
 #h(x) axes x1y2

