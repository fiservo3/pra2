set encoding utf8

set xlabel "a [cm]"
set ylabel "a' [cm]"

set grid

plot \
'./pruseciky.txt' title "průsečíky spojic",\
 x dt 2 lc rgb "black" title "osa kvadrantu",\
 './prumery.txt' using 1:2:3:4 pt 4 with xyerrorbars title "průměr"