import math
import numpy as np
import scipy.special

def prusecik(k1, q1, k2, q2):
	x = (q2-q1)/(k1-k2)
	y = k1*x + q1
	return((x, y))

def prusecikPrimek(a, b):
	k1 = data[a,0]
	k2 = data[b,0]
	q1 = data[a,1]
	q2 = data[b,1]
	return(prusecik(k1, q1, k2, q2))

data = np.loadtxt("primky.txt") 
n = data.shape[0]

pruseciky = np.zeros((scipy.special.binom(5, 2).astype(int) ,2))



k = 0

for i in range(0, n+1):
	for j in range(i+1, n):
		#print(prusecikPrimek(i, j))
		pruseciky[k, :] = prusecikPrimek(i, j)
		k += 1
		#print (i, j)	#debug
print(pruseciky)

print("x = " + str(pruseciky[:,0].mean()) + " +- " + str(pruseciky[:,0].std()))
print("y = " + str(pruseciky[:,1].mean()) + " +- " + str(pruseciky[:,1].std()))