import numpy as np
import math

data = np.loadtxt("data_prumer.txt")
n=data.shape[0]
vahy = np.zeros((n, 1)) 
odchylky = np.zeros((n, 1)) 

for i in range(0,n):
	vahy[i] = 1/(data[i, 1]**2) 

suma = 0
for i in range(0,n):
	suma += vahy[i] * data[i,0]



prumer = suma/vahy.sum()

suma2 = 0
for i in range(0,n):
	suma2 += vahy[i] * (data[i,0] - prumer)**2

sigma = math.sqrt(suma2/vahy.sum())

print(str(prumer) + " +- " + str(sigma) )