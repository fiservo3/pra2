set encoding utf8

m_e = 1.8e11

R = 0.15
N = 130
el = 0.381
pi = 3.14159265359
mu = 1.25663706e-6
d = 0.163

data = "rtut_spocitany.txt"

nn = lambdan = C = 1

f(x) = nn + C/(x - lambdan)

fit f(x) data using 1:2 via nn, lambdan, C

set xlabel "λ [nm]"
set ylabel "n [1]"

set key top left

plot data using 1:2 with points title "hodnoty", \
f(x) title "fit"