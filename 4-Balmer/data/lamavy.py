import numpy as np;
import os;
import math;


def uhel(x):
	(m, s) = math.modf(x);
	m=m*60;
	(v, m) = math.modf(m);
	v=v*60;

	print(str(s) + "° " + str(m) + "' " + str(v) + "''");



data = np.loadtxt("lamavy_uhel.txt");

n=data.shape[0];

z=np.zeros((n, 1));
suma = 0;
suma_odchylek = 0;
i=0;
while i<n:
	z[i] = data[i, 0] + (1/60)*data[i, 1] + (1/3600)*data[i, 2];
	suma += z[i];
	i+=1;

prumer = suma/n

for i in range(1, n):
	suma_odchylek += (prumer - z[i])**2;

sigma = math.sqrt(suma_odchylek/(n*(n-1)))


#print(z)
print(uhel(prumer));
print(uhel(sigma));



