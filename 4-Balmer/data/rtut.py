import numpy as np;
import os;
import math;

phi = 60 + (1./15)

def uhel(x):
	(m, s) = math.modf(x);
	m=m*60;
	(v, m) = math.modf(m);
	v=v*60;

	print(str(s) + "° " + str(m) + "' " + str(v) + "''");



dataA = np.loadtxt("rtutA.txt");
dataB = np.loadtxt("rtutB.txt");


if dataA.shape[0] == dataB.shape[0]:
	n = dataA.shape[0];
	#pro jistotu
	n = dataB.shape[0];


z = np.zeros((n, 3));

print(z.shape)
print(dataA.shape)

for i in np.arange(0,n):
	z[i,0] = dataA[i, 0];
	A = dataA[i, 0] + (1/60)*dataA[i, 1] + (1/3600)*dataA[i, 2];
	B = dataB[i, 0] + (1/60)*dataB[i, 1] + (1/3600)*dataB[i, 2];
	epsilon = (A-B)/2
	n = (math.sin((epsilon + phi)/2))/(math.sin((phi)/2))
	z[i,1] = n
	z[i,2] = (1./12.)

np.savetxt("rtut_spocitany.txt", z)