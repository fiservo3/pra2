set encoding utf8

data = "cejchovani.txt"

set key top left

set xlabel "U [V]"
set ylabel "T [K]"

plot data using 1:2 with points