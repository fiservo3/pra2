set encoding utf8

data = "cejchovani.txt"

k=1e-10
f(x) = k * x**4
fit f(x) data using 2:($1*$3) via k

set key top left

set ylabel "P [W]"
set xlabel "T [K]"

plot data using 2:($1*$3) with points title "změřené hodnoty",\
f(x) title "fit"