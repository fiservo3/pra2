import math
import numpy as np

ls = 0.046130381919148845

def T(rho):
	q0 = -1.73573
	polynom = [1.1167e-13, 1.163983e-9, 5.7481110e-6, 2.14350e-2, q0-rho]
	x = np.roots(polynom)
	#print(x)
	return(x[3].real)

data = np.loadtxt("data.txt")
n = data.shape[0];

z = np.zeros((n, 3))
z[:,0] = data[:,0]
z[:,2] = data[:,1]

for i in np.arange(0, n):
	R = data[i, 0]/data[i, 1]
	rho = R/ls
	print(str(data[i, 0]) + ", " + str(T(rho)))
	z[i,1] = T(rho)

np.savetxt("cejchovani.txt", z)

#print(data)