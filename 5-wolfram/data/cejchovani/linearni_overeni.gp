set encoding utf8

k=1
q=1

Ro = 0.25
alpha = 1
To = 295.65
f(x) = k*x + q 
R(x) = Ro*(alpha * (x-To) + 1)

data = "cejchovani.txt"

fit f(x) data using 2:($1/$3) via k, q
fit R(x) data using 2:($1/$3)via alpha

set key top left

set ylabel "R [Ω]"
set xlabel "T [K]"

set yrange [0:3.1]

plot data using 2:($1/$3)with points title "naměřené hodnoty svítící žárovky",\
[830:1830] f(x) title "fit svítících" dt 3,\
R(x) title "fit s výzhozím bodem" ,\
"pom.txt" using 1:2 title "hodnota s nulovým výkonem"