import numpy as np
import math

def T(P):
	beta = 4.12173e-12
	return (P/beta)**0.25

def lmbda(n):
    return {
        1: 420,
        2: 430,
        3: 470,
        4: 500,
        5: 530,
        6: 570,
        7: 610,
        8: 660,
        9: 720,
        10: 750,
    }[n]

def _planck(lbda, T):
	h = 6.62607004081e-34
	c = 299792458
	k = 1.38064852e-23

	#print(h)
	_i = 1/(math.exp((h*c)/(lbda*k*T))-1)
	return(_i)


def planck(lbda, T):
	h = 6.62607004081e-34
	c = 299792458
	k = 1.38064852e-23
	dlamba = 30
	#print(h)
	i = (dlamba * 2 * h * c**2)/((math.exp((h*c)/(lbda*k*T))-1)*lbda**5)
	return(i)



soubor = "6.txt"
T0 = 4# pm 0.5
U = 12.49
I = 4.05

P = U*I

data = np.loadtxt(soubor)
n = data.shape[0]
z = np.zeros((n, 2))

for i in np.arange(0, n):
	lbda = lmbda(data[i, 0])
	z[i, 0] = lbda
	z[i, 1] =  data[i, 1] * planck(lbda, T(P))
print (z)


print(P)
np.savetxt("prepocteno6.txt", z)
