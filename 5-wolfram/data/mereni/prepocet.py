import numpy as np
import math

def lmbda(n):
    return {
        1: 420,
        2: 430,
        3: 470,
        4: 500,
        5: 530,
        6: 570,
        7: 610,
        8: 660,
        9: 720,
        10: 750,
    }[n]

print(lmbda(3))

