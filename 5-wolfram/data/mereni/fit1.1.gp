set encoding utf8

data = "prepocteno1.1.txt"
trans = 4.0
theta = 1300.0

h = 6.62607004081e-34
c = 299792458.0
k = 1.38064852e-23
dlambda = 30

_planck(x) = (trans*dlambda*2*h*c**2)/((exp((h*c)/(x*k*theta))-1.0)*x**5)


fit _planck(x) data using 1:2 via theta

set key top right

set xlabel "λ [nm]"
set ylabel "I [cd]"

plot data using 1:2 with points title "spočtené hodnoty",\
_planck(x) title "fit planckova zákona"