import math

A = -1.06871
B = 2.06884e-2
C = 1.27971e-6
D = 8.53101e-9
E = 5.14195e-12

T = 273.15 + 22.5
sigma_T = 0.1

R = 0.25
sigma_R = 0.01


rho = A + B*T + C*T**2 + D*T**3 + E*T**4
sigma_rho = sigma_T * (B + 2*C*T + 3*D*T**2 + 4*E*T**3)
print("rho: ")
print(rho)
print("pm")
print(sigma_rho)


ls = R/rho
sigma_ls = math.sqrt(sigma_rho**2*R**4 + sigma_R*2)/rho**2

print("l/S: ")
print(ls)
print("pm")
print(sigma_ls)