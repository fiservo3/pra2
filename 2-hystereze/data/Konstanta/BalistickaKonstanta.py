import numpy as np;
import os;

Lref = 7.27e-3;
R = 9000;

data = np.loadtxt("dataKonstanta.txt");
n=data.shape[0]
print (n)

z=np.zeros((n, 2))

suma = 0;


i = 0;
while i<n:
	z[i, 0] = data [i, 0];
	z[i, 1] = (2*Lref*data [i, 0])/(data [i, 1]*R);
	suma += z[i, 1];
	i += 1;

np.savetxt("dataKonstanta_prep.txt", z);

print("aritmetický průměr Rb*lambda = " + str(suma/n))

os.system("gnuplot ./pomocny_plot.gp --persist")