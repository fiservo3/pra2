set encoding utf8
set fit errorscaling limit 1e-10
data="./data.txt"

KbLambda = 1e-5
Lref = 7.27e-3
R = 9000

s(x) = x*((2*Lref)/(R*KbLambda))
fit s(x) data using 1:2 via KbLambda

set xlabel "I [A]"
set ylabel "s [cm]"
set xrange [-0.21:0.21]

plot data using 1:2:3 with yerrorbars title "Změřené hodnoty", s(x) title "lineární fit"