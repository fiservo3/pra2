set encoding utf8

file_A="data_A.txt"
file_B="data_B.txt"


Br = 1
kf = 1
f(x) = Br + kf*x

fit [-15:15]  f(x) file_A via Br, kf


Hk = 1
kg = 1
g(x) = kg*(x - Hk)

fit [8:40] g(x) file_B via Hk, kg

set xlabel "H [A/m]"
set ylabel "B [T]"

set key top left
set xzeroaxis
set yzeroaxis

set xrange [-75:75]
set yrange [-0.2:0.25]

plot file_A using 1:2:3 with yerrorbars title "magnetizační křivka", \
file_B using 1:2:3 with yerrorbars title "magnetizační křivka", \
[-15:15] f(x) lt rgb "blue" lw 1.5 notitle, f(x) dt 2 lw 0.5  lt rgb "blue", \
[8:40] g(x) lt rgb "red" lw 1.5 notitle, g(x) dt 3 lw 0.5  lt rgb "red"
