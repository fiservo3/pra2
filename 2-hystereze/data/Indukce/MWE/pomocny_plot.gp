set encoding utf8

file_A="data_A.txt"
file_B="data_B.txt"

set xlabel "H [A/m]"
set ylabel "B [T]"

set key top left

plot file_A using 1:2:3 with yerrorbars title "křivka magnetizace na jednu stranu", file_B using 1:2:3 with yerrorbars title "křivka magnetizace na druhou stranu"