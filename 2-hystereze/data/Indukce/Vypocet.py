import numpy as np;
import os;

pi = 3.14159265359;
r=17.1e-3;
S=24.3e-6;


Rmer = 9000;
n1 = 62;
n2 = 400;

KbLambda = 1.54544e-08;

Bmax = 0.2561423703703703
Imax = 0.204;
#chyba, kterou spocetl kolega
chyba = 0.01

dataA1 = np.loadtxt("data1.txt");
dataA2 = np.loadtxt("data2.txt");

dataB1 = np.loadtxt("data3.txt");
dataB2 = np.loadtxt("data4.txt");
m=dataA1.shape[0]
n=dataA2.shape[0]
o=dataB1.shape[0]
p=dataB2.shape[0]

print("nacteno dat:")
print (str(n) +", " + str(m))
print (str(o) +", " + str(p))

zA=np.zeros((n+m, 3))
zB=np.zeros((o+p, 3))


#pro prvni dva useky - krivka jednim smerem
i = 0;
while i<m:
	H = (n1*dataA1[i, 0])/(2*pi*r);
	Q = dataA1[i, 1]*KbLambda;
	B = Bmax - (Rmer*Q)/(n2*S);

	zA[i, 0] = H;
	zA[i, 1] = B;


	i += 1;

i = 0;
while i<n:
	H = -(n1*(dataA2[i, 0]))/(2*pi*r);
	Q = dataA2 [i, 1]*KbLambda;
	B = Bmax - (Rmer*Q)/(n2*S);

	zA[m+i, 0] = H;
	zA[m+i, 1] = B;

	i += 1;


#pro druhe dva useky - krivka druhym

i = 0;
while i<o:
	H = -(n1*dataB1[i, 0])/(2*pi*r);
	Q = dataB1[i, 1]*KbLambda;
	B = (Rmer*Q)/(n2*S) -Bmax;

	zB[i, 0] = H;
	zB[i, 1] = B;

	i += 1;

i = 0;
while i<p:
	H = (n1*(dataB2[i, 0]))/(2*pi*r);
	Q = dataB2 [i, 1]*KbLambda;
	B = (Rmer*Q)/(n2*S) - Bmax;

	zB[o+i, 0] = H;
	zB[o+i, 1] = B;

	i += 1;


#doplnim chybu určenou metodou vycucání z prstu

zA[:,2] = chyba;
zB[:,2] = chyba;

#print (zA)
np.savetxt("data_A.txt", zA);
np.savetxt("data_B.txt", zB);

I1 = np.trapz(zA[:,1], zA[:,0]);
I2 = np.trapz(zB[:,1], zB[:,0]);

print("Integral horni:" + str(I1));
print("Integral dolni:" + str(I2));
print("Rozdil integralu:" + str(I1-I2));





os.system("gnuplot ./pomocny_plot.gp --persist")