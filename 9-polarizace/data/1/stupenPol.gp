set encoding utf8

set angles degrees
set samples 10000
data = 'spoc.txt'

#alfa = 20
#
#f(x) = alfa*sin(x+45)**2
#
#fit f(x) data using 1:2:3:4 xyerrors via alfa
#
#set grid
set xrange [15:65]
#
set xlabel "α [°]"
set ylabel "||P|| [-]"
set grid
plot \
data using 1:2:3 with yerrorbars title "data",\
#f(x) title "fit"