import numpy as np
import math

data = np.loadtxt("vypocet.txt")
n=data.shape[0]

z = np.zeros((n, 2))
z[:,0] = data[:,0]

j = 0
for i in data:
	sigmaE2 = 1

	stEx2 = i[1]
	stEy2 = i[2]
	stExEy = (i[3] - 0.5*stEy2 - 0.5*stEx2)
	sigmaExEy = math.sqrt(2* sigmaE2**2 )
	silenost = (i[4] - 0.5*stEy2 - 0.5*stEx2)
	sigmaSilenost = sigmaExEy
	jmenovtel = stEx2 + stEy2
	sigmaJmeno = math.sqrt(2) * sigmaE2

	P1 = (stEx2- stEy2)/jmenovtel
	P2 = (2*stExEy)/jmenovtel
	P3 = (2*silenost)/jmenovtel
	P = np.zeros((1,3)) 

	sigmaP1 = math.sqrt((sigmaJmeno/jmenovtel)**2+(sigmaJmeno*(stEx2- stEy2)/jmenovtel**2)**2)
	sigmaP2 = math.sqrt((2*sigmaExEy/jmenovtel)**2+(sigmaJmeno*(2*stExEy)/jmenovtel**2)**2)
	sigmaP3 = math.sqrt((2*sigmaSilenost/jmenovtel)**2+(sigmaJmeno*(2*silenost)/jmenovtel**2)**2)



	P[:] = (P1, P2, P3)

	velikost = math.sqrt(P1**2 + P2**2 + P3**2)
	sigmaVel = math.sqrt((sigmaP1*P1/velikost)**2 + (sigmaP2*P2/velikost)**2 + (sigmaP3*P3/velikost)**2)

	#print(i[1], i[2], i[3], i[4])
	##print(stEx2, stEy2, stExEy, silenost)
	##print(P1, P2, P3)
	print(str(i[0]) + ": " + str(velikost) + " pm " + str(sigmaVel))
	