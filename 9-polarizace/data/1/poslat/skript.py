import numpy as np
import math

data = np.loadtxt("vypocet.txt")
n=data.shape[0]

z = np.zeros((n, 2))
z[:,0] = data[:,0]

for i in data:

	stEx2 = i[1]
	stEy2 = i[2]
	stExEy = (i[3] - 0.5*stEy2 - 0.5*stEx2)
	silenost = (i[4] - 0.5*stEy2 - 0.5*stEx2)

	jmenovtel = stEx2 + stEy2

	P1 = (stEx2- stEy2)/jmenovtel
	P2 = (2*stExEy)/jmenovtel
	P3 = (2*silenost)/jmenovtel
	
	
	P = np.zeros((1,3)) 
	P[:] = (P1, P2, P3)

	velikost = math.sqrt(P1**2 + P2**2 + P3**2)
	print(i[1], i[2], i[3], i[4])
	print(stEx2, stEy2, stExEy, silenost)
	print(P1, P2, P3)
	print(velikost)
	print(np.linalg.norm(P))
