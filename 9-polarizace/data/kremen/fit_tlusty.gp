set encoding utf8

set angles degrees
set samples 10000
dataA = 'FModra_Srsi.txt'

AA = 1
BA = 1
CA = -129.2

fA(x) = AA*cos(x+CA)**2 + BA

fit fA(x) dataA using 1:2:3:4 xyerrors via AA, BA, CA

set grid
set xrange [-5:65]

set xlabel "θ [°]"
set ylabel "U [mV]"

set key bottom center

plot \
dataA using 1:2:3:4 with xyerrorbars title "data" lc rgb "blue",\
[-3:63]fA(x) title "fit" lc rgb "blue"
