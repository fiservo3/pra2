set encoding utf8

set angles degrees
set samples 10000
dataA = 'FCervena.txt'
dataB = 'FOrang.txt'
dataC = 'FZelen.txt'
dataD = 'FModra.txt'

AA = 1
BA = 1
CA = 1

AB = 1
BB = 1
CB = 1

AC = 1
BC = 1
CC = -40

AD = 1
BD = 1
CD = 1

fA(x) = AA*cos(x+CA)**2 + BA
fB(x) = AB*cos(x+CB)**2 + BB
fC(x) = AC*cos(x+CC)**2 + BC
fD(x) = AD*cos(x+CD)**2 + BD

fit fA(x) dataA using 1:2:3:4 xyerrors via AA, BA, CA
fit fB(x) dataB using 1:2:3:4 xyerrors via AB, BB, CB
fit fC(x) dataC using 1:2:3:4 xyerrors via AC, BC, CC
fit fD(x) dataD using 1:2:3:4 xyerrors via AD, BD, CD

set grid
set xrange [-5:80]

set xlabel "θ [°]"
set ylabel "U [mV]"

set key bottom right

plot \
dataA using 1:2:3:4 with xyerrorbars title "data 1" lc rgb "red",\
dataB using 1:2:3:4 with xyerrorbars title "data 2" lc rgb "orange",\
dataC using 1:2:3:4 with xyerrorbars title "data 3" lc rgb "green",\
dataD using 1:2:3:4 with xyerrorbars title "data 4" lc rgb "blue",\
[-3:63]fA(x) title "fit 1" lc rgb "red",\
[-3:63]fB(x) title "fit 2" lc rgb "orange",\
[-3:63]fC(x) title "fit 3" lc rgb "green",\
[-3:73]fD(x) title "fit 4" lc rgb "blue"