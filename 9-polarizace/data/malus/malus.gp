set encoding utf8

set angles degrees
set samples 10000
data = 'data.txt'

alfa = 20

f(x) = alfa*sin(x+45)**2

fit f(x) data using 1:2:3:4 xyerrors via alfa

set grid
set xrange [-8:98]

set xlabel "θ [°]"
set ylabel "U [mV]"

plot \
data using 1:2:3:4 with xyerrorbars title "data",\
f(x) title "fit"